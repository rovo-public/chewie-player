import 'dart:async';

import 'package:chewie/src/chewie_player.dart';
import 'package:chewie/src/chewie_progress_colors.dart';
import 'package:chewie/src/rovo_progress_bar.dart';
import 'package:chewie/src/utils.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class RovoControls extends StatefulWidget {
  final Color backgroundColor;
  final Color iconColor;
  final Widget overlayWidget;
  final bool showProgress;
  final bool showTimer;
  final bool showPlayPause;

  const RovoControls({
    Key? key,
    required this.backgroundColor,
    required this.iconColor,
    required this.overlayWidget,
    this.showProgress = true,
    this.showTimer = true,
    this.showPlayPause = true,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _RovoControlsState();
  }
}

class _RovoControlsState extends State<RovoControls> {
  late VideoPlayerValue _latestValue;
  late double _latestVolume;
  late bool _hideStuff;
  late Timer _hideTimer = Timer(const Duration(seconds: 0), () {});
  late Timer _initTimer = Timer(const Duration(seconds: 0), () {});
  late Timer _showAfterExpandCollapseTimer = Timer(const Duration(seconds: 0), () {});
  bool _dragging = false;
  bool _displayTapped = false;

  final barHeight = 48.0;
  final marginSize = 5.0;

  late VideoPlayerController controller;
  ChewieController? chewieController;

  @override
  void initState() {
    super.initState();
    _hideStuff = widget.showPlayPause;
  }

  @override
  Widget build(BuildContext context) {
    if (chewieController == null) return Container();
    if (_latestValue.hasError) {
      return chewieController!.errorBuilder != null
          ? chewieController!.errorBuilder!(
              context,
              chewieController!.videoPlayerController.value.errorDescription ?? '',
            )
          : Center(
              child: Icon(
                Icons.error,
                color: Colors.white,
                size: 42,
              ),
            );
    }

    final backgroundColor = widget.backgroundColor;
    final iconColor = widget.iconColor;

    return MouseRegion(
      onHover: (_) {
        _cancelAndRestartTimer();
      },
      child: GestureDetector(
        onTap: () => _cancelAndRestartTimer(),
        child: AbsorbPointer(
          absorbing: _hideStuff,
          child: Column(
            children: <Widget>[
              _latestValue != null &&
                          !_latestValue.isPlaying &&
                          _latestValue.duration == null ||
                      _latestValue.isBuffering
                  ? const Expanded(
                      child: const Center(
                        child: const CircularProgressIndicator(),
                      ),
                    )
                  : Expanded(
                      child: Stack(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.center,
                            child: (chewieController!
                                    .videoPlayerController.value.isPlaying)
                                ? AnimatedOpacity(
                                    opacity: _hideStuff ? 0.0 : 1.0,
                                    duration: Duration(milliseconds: 300),
                                    child: _buildPlayPauseButton(),
                                  )
                                : _buildPlayPauseButton(),
                          ),
                          if (chewieController!.isFullScreen)
                            _buildOverlay(context, widget.overlayWidget,
                                iconColor, widget.showTimer),
                          _buildHitArea(),
                          Positioned(
                            bottom: 0.0,
                            right: 8.0,
                            child: chewieController!.allowFullScreen
                                ? (chewieController!
                                        .videoPlayerController.value.isPlaying)
                                    ? AnimatedOpacity(
                                        opacity: _hideStuff ? 0.0 : 1.0,
                                        duration: Duration(milliseconds: 300),
                                        child: _buildExpandButton(iconColor),
                                      )
                                    : _buildExpandButton(iconColor)
                                : Container(),
                          ),
                        ],
                      ),
                    ),
              if (widget.showProgress) _buildProgressBar()
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _dispose();
    super.dispose();
  }

  void _dispose() {
    controller.removeListener(_updateState);
    _hideTimer.cancel();
    _initTimer.cancel();
    _showAfterExpandCollapseTimer.cancel();
  }

  @override
  void didChangeDependencies() {
    final _oldController = chewieController;
    chewieController = ChewieController.of(context);

    if (chewieController != null) {
      controller = chewieController!.videoPlayerController;
    }

    if (_oldController != chewieController) {
      _dispose();
      _initialize();
    }

    super.didChangeDependencies();
  }

  Widget _buildPlayPauseButton() {
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Container(
          color: Colors.black26,
        ),
        InkWell(
          borderRadius: BorderRadius.circular(50.0),
          onTap: () => chewieController!.videoPlayerController.value.isPlaying
              ? chewieController!.pause()
              : chewieController!.play(),
          child: Container(
            width: 50,
            height: 50,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Color(0xFF0AC2CC),
            ),
            padding: EdgeInsets.all(8.0),
            alignment: Alignment.center,
            child: Image.asset(
              chewieController!.videoPlayerController.value.isPlaying
                  ? 'assets/images/icon_pause.png'
                  : 'assets/images/icon_play.png',
              color: Colors.white,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildOverlay(
    BuildContext context,
    Widget widget,
    Color iconColor,
    bool showTimer,
  ) {
    final position = _latestValue != null && _latestValue.position != null
        ? _latestValue.position
        : Duration.zero;

    return Row(
      children: <Widget>[
        Expanded(child: widget ?? Container()),
        if (showTimer)
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              '${formatDuration(position)}',
              style: TextStyle(
                fontFamily: 'Roboto',
                fontWeight: FontWeight.normal,
                fontSize: 36,
                color: iconColor,
              ),
            ),
          )
      ],
    );
  }

  GestureDetector _buildExpandButton(Color iconColor) {
    return GestureDetector(
      onTap: _onExpandCollapse,
      child: AnimatedOpacity(
        opacity: _hideStuff ? 0.0 : 1.0,
        duration: Duration(milliseconds: 300),
        child: Container(
          height: barHeight,
          padding: EdgeInsets.symmetric(horizontal: 16.0),
          child: Center(
            child: Icon(
              chewieController!.isFullScreen
                  ? Icons.fullscreen_exit
                  : Icons.fullscreen,
              color: iconColor,
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildHitArea() {
    return GestureDetector(
      onTap: () {
        if (_latestValue != null && _latestValue.isPlaying) {
          if (_displayTapped) {
            setState(() {
              _hideStuff = true;
            });
          } else
            _cancelAndRestartTimer();
        } else {
          _playPause();

          setState(() {
            _hideStuff = true;
          });
        }
      },
      child: Container(),
    );
  }

  GestureDetector _buildMuteButton(
    VideoPlayerController controller,
    Color iconColor,
  ) {
    return GestureDetector(
      onTap: () {
        _cancelAndRestartTimer();

        if (_latestValue.volume == 0) {
          controller.setVolume(_latestVolume ?? 0.5);
        } else {
          _latestVolume = controller.value.volume;
          controller.setVolume(0.0);
        }
      },
      child: AnimatedOpacity(
        opacity: _hideStuff ? 0.0 : 1.0,
        duration: Duration(milliseconds: 300),
        child: ClipRect(
          child: Container(
            child: Container(
              height: barHeight,
              padding: EdgeInsets.only(
                left: 8.0,
                right: 8.0,
              ),
              child: Icon(
                (_latestValue != null && _latestValue.volume > 0)
                    ? Icons.volume_up
                    : Icons.volume_off,
                color: iconColor,
              ),
            ),
          ),
        ),
      ),
    );
  }

  GestureDetector _buildPlayPause(
      VideoPlayerController controller, Color iconColor) {
    return GestureDetector(
      onTap: _playPause,
      child: Container(
        height: barHeight,
        color: Colors.transparent,
        margin: EdgeInsets.only(left: 8.0, right: 4.0),
        padding: EdgeInsets.only(
          left: 12.0,
          right: 12.0,
        ),
        child: Icon(
          controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
          color: iconColor,
        ),
      ),
    );
  }

  void _cancelAndRestartTimer() {
    _hideTimer?.cancel();
    _startHideTimer();

    setState(() {
      _hideStuff = false;
      _displayTapped = true;
    });
  }

  Future<Null> _initialize() async {
    controller.addListener(_updateState);

    _updateState();

    if ((controller.value != null && controller.value.isPlaying) ||
        chewieController!.autoPlay) {
      _startHideTimer();
    }

    if (chewieController!.showControlsOnInitialize) {
      _initTimer = Timer(Duration(milliseconds: 200), () {
        setState(() {
          _hideStuff = false;
        });
      });
    }
  }

  void _onExpandCollapse() {
    setState(() {
      _hideStuff = true;

      chewieController!.toggleFullScreen();
      _showAfterExpandCollapseTimer = Timer(Duration(milliseconds: 300), () {
        setState(() {
          _cancelAndRestartTimer();
        });
      });
    });
  }

  void _playPause() {
    bool isFinished = _latestValue.position >= _latestValue.duration;

    setState(() {
      if (controller.value.isPlaying) {
        _hideStuff = false;
        _hideTimer?.cancel();
        controller.pause();
      } else {
        _cancelAndRestartTimer();

        if (!controller.value.isInitialized) {
          controller.initialize().then((_) {
            controller.play();
          });
        } else {
          if (isFinished) {
            controller.seekTo(Duration(seconds: 0));
          }
          controller.play();
        }
      }
    });
  }

  void _startHideTimer() {
    _hideTimer = Timer(const Duration(seconds: 3), () {
      setState(() {
        _hideStuff = true;
      });
    });
  }

  void _updateState() {
    setState(() {
      _latestValue = controller.value;
    });
  }

  Widget _buildProgressBar() {
    return RovoVideoProgressBar(
      controller,
      onDragStart: () {
        setState(() {
          _dragging = true;
        });

        _hideTimer?.cancel();
      },
      onDragEnd: () {
        setState(() {
          _dragging = false;
        });

        _startHideTimer();
      },
      colors: chewieController!.materialProgressColors, onDragUpdate: () {  },
    );
  }
}
