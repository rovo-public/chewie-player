import 'dart:async';

import 'package:auto_orientation/auto_orientation.dart';
import 'package:chewie/chewie.dart';
import 'package:chewie/src/chewie_progress_colors.dart';
import 'package:chewie/src/player_with_controls.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:video_player/video_player.dart';
import 'package:wakelock_plus/wakelock_plus.dart';

typedef Widget ChewieRoutePageBuilder(
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    _ChewieControllerProvider controllerProvider);

/// A Video Player with Material and Cupertino skins.
///
/// `video_player` is pretty low level. Chewie wraps it in a friendly skin to
/// make it easy to use!
class Chewie extends StatefulWidget {
  Chewie({
    Key? key,
    required this.controller,
  })  : super(key: key);

  /// The [ChewieController]
  final ChewieController controller;

  @override
  ChewieState createState() {
    return ChewieState();
  }
}

class ChewieState extends State<Chewie> {
  bool _isFullScreen = false;

  @override
  void initState() {
    super.initState();
    widget.controller.addListener(listener);
  }

  @override
  void dispose() {
    widget.controller.removeListener(listener);
    super.dispose();
  }

  @override
  void didUpdateWidget(Chewie oldWidget) {
    if (oldWidget.controller != widget.controller) {
      widget.controller.addListener(listener);
    }
    super.didUpdateWidget(oldWidget);
  }

  void listener() async {
    if (widget.controller.isFullScreen && !_isFullScreen) {
      _isFullScreen = true;
      await _pushFullScreenWidget(context);
    } else if (_isFullScreen) {
      Navigator.of(context, rootNavigator: true).pop();
      _isFullScreen = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return _ChewieControllerProvider(
      controller: widget.controller,
      child: PlayerWithControls(),
    );
  }

  Widget _buildFullScreenVideo(
      BuildContext context,
      Animation<double> animation,
      _ChewieControllerProvider controllerProvider) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        alignment: Alignment.center,
        color: Colors.black,
        child: controllerProvider,
      ),
    );
  }

  AnimatedWidget _defaultRoutePageBuilder(
      BuildContext context,
      Animation<double> animation,
      Animation<double> secondaryAnimation,
      _ChewieControllerProvider controllerProvider) {
    return AnimatedBuilder(
      animation: animation,
      builder: (BuildContext context, Widget? child) {
        return _buildFullScreenVideo(context, animation, controllerProvider);
      },
    );
  }

  Widget _fullScreenRoutePageBuilder(
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
  ) {
    var controllerProvider = _ChewieControllerProvider(
      controller: widget.controller,
      child: PlayerWithControls(),
    );

    if (widget.controller.routePageBuilder == null) {
      return _defaultRoutePageBuilder(
          context, animation, secondaryAnimation, controllerProvider);
    }
    return widget.controller.routePageBuilder!(
        context, animation, secondaryAnimation, controllerProvider);
  }

  Future<dynamic> _pushFullScreenWidget(BuildContext context) async {
    final TransitionRoute<Null> route = PageRouteBuilder<Null>(
      pageBuilder: _fullScreenRoutePageBuilder,
    );

    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
    AutoOrientation.landscapeRightMode();

    if (!widget.controller.allowedScreenSleep) {
      WakelockPlus.enable();
    }

    await Navigator.of(context, rootNavigator: true).push(route);
    _isFullScreen = false;
    widget.controller.exitFullScreen();

    // The wakelock plugins checks whether it needs to perform an action internally,
    // so we do not need to check Wakelock.isEnabled.
    WakelockPlus.disable();

    SystemChrome.setEnabledSystemUIMode(
        SystemUiMode.manual, overlays: widget.controller.systemOverlaysAfterFullScreen);
    SystemChrome.setPreferredOrientations(
        widget.controller.deviceOrientationsAfterFullScreen);

    AutoOrientation.portraitUpMode();
  }
}

enum ControlAlignment {
  TopLeft,
  TopRight,
  BottomLeft,
  BottomRight,
}

/// The ChewieController is used to configure and drive the Chewie Player
/// Widgets. It provides methods to control playback, such as [pause] and
/// [play], as well as methods that control the visual appearance of the player,
/// such as [enterFullScreen] or [exitFullScreen].
///
/// In addition, you can listen to the ChewieController for presentational
/// changes, such as entering and exiting full screen mode. To listen for
/// changes to the playback, such as a change to the seek position of the
/// player, please use the standard information provided by the
/// `VideoPlayerController`.
class ChewieController extends ChangeNotifier {
  ChewieController({
    required this.videoPlayerController,
    double? aspectRatio,
    this.autoInitialize = false,
    this.autoPlay = false,
    Duration? startAt,
    this.looping = false,
    this.fullScreenByDefault = false,
    ChewieProgressColors? cupertinoProgressColors,
    ChewieProgressColors? materialProgressColors,
    Widget? placeholder,
    Widget? overlay,
    this.showControlsOnInitialize = true,
    this.showControls = true,
    Widget? customControls,
    Widget Function(BuildContext context, String errorMessage)? errorBuilder,
    this.allowedScreenSleep = true,
    this.isLive = false,
    this.allowFullScreen = true,
    this.allowMuting = true,
    this.allowVoiceOver = true,
    this.showDots = false,
    this.showReplay = false,
    this.showPause = false,
    this.controlAlignment = ControlAlignment.TopRight,
    Function()? onReplayVideo,
    Function(bool)? onPausePlayVideo,
    Function()? onEndVideo,
    this.playDuringTransition = false,
    FlutterTts? tts,
    this.startCommand = '',
    this.endCommand = '',
    Function(bool)? playCallback,
    this.systemOverlaysAfterFullScreen = SystemUiOverlay.values,
    this.deviceOrientationsAfterFullScreen = const [
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ],
    ChewieRoutePageBuilder? routePageBuilder,
    this.delayDuration = 0,
    this.startTime = 0,
    this.endTime = 0,
  }) {
    _initialize();

    if (tts != null) {
      this.tts = tts;
    }
    if (aspectRatio != null) {
      this.aspectRatio = aspectRatio;
    }
    if (startAt != null) {
      this.startAt = startAt;
    }
    if (cupertinoProgressColors != null) {
      this.cupertinoProgressColors = cupertinoProgressColors;
    }
    if (materialProgressColors != null) {
      this.materialProgressColors = materialProgressColors;
    }
    if (placeholder != null) {
      this.placeholder = placeholder;
    }
    if (overlay != null) {
      this.overlay = overlay;
    }
    if (customControls != null) {
      this.customControls = customControls;
    }
    if (errorBuilder != null) {
      this.errorBuilder = errorBuilder;
    }
    if (onReplayVideo != null) {
      this.onReplayVideo = onReplayVideo;
    }
    if (onPausePlayVideo != null) {
      this.onPausePlayVideo = onPausePlayVideo;
    }
    if (onEndVideo != null) {
      this.onEndVideo = onEndVideo;
    }
    if (playCallback != null) {
      this.playCallback = playCallback;
    }
    if (routePageBuilder != null) {
      this.routePageBuilder = routePageBuilder;
    }
  }

  /// The controller for the video you want to play
  VideoPlayerController videoPlayerController;

  /// Initialize the Video on Startup. This will prep the video for playback.
  final bool autoInitialize;

  /// Play the video as soon as it's displayed
  final bool autoPlay;

  /// Start video at a certain position
  Duration? startAt;

  /// Whether or not the video should loop
  final bool looping;

  /// Weather or not to show the controls when initializing the widget.
  final bool showControlsOnInitialize;

  /// Whether or not to show the controls at all
  final bool showControls;

  /// Defines customised controls. Check [MaterialControls] or
  /// [CupertinoControls] for reference.
  Widget? customControls;

  /// When the video playback runs  into an error, you can build a custom
  /// error message.
  Widget Function(BuildContext context, String errorMessage)? errorBuilder;

  /// The Aspect Ratio of the Video. Important to get the correct size of the
  /// video!
  ///
  /// Will fallback to fitting within the space allowed.
  double? aspectRatio;

  /// The colors to use for controls on iOS. By default, the iOS player uses
  /// colors sampled from the original iOS 11 designs.
  ChewieProgressColors? cupertinoProgressColors;

  /// The colors to use for the Material Progress Bar. By default, the Material
  /// player uses the colors from your Theme.
  ChewieProgressColors? materialProgressColors;

  /// The placeholder is displayed underneath the Video before it is initialized
  /// or played.
  Widget? placeholder;

  /// A widget which is placed between the video and the controls
  Widget? overlay;

  /// Defines if the player will start in fullscreen when play is pressed
  final bool fullScreenByDefault;

  /// Defines if the player will sleep in fullscreen or not
  final bool allowedScreenSleep;

  /// Defines if the controls should be for live stream video
  final bool isLive;

  /// Defines if the fullscreen control should be shown
  final bool allowFullScreen;

  /// Defines if the mute control should be shown
  final bool allowMuting;

  /// Defines the system overlays visible after exiting fullscreen
  final List<SystemUiOverlay> systemOverlaysAfterFullScreen;

  /// Defines the set of allowed device orientations after exiting fullscreen
  final List<DeviceOrientation> deviceOrientationsAfterFullScreen;

  /// Defines a custom RoutePageBuilder for the fullscreen
  ChewieRoutePageBuilder? routePageBuilder;

  final int delayDuration;

  FlutterTts? tts;

  final String endCommand;

  final String startCommand;

  Function(bool isPlaying)? playCallback;

  int startTime;

  int endTime;

  static ChewieController of(BuildContext context) {
    final chewieControllerProvider =
        context.dependOnInheritedWidgetOfExactType<_ChewieControllerProvider>()
            as _ChewieControllerProvider;

    return chewieControllerProvider.controller;
  }

  bool _isFullScreen = false;
  bool allowVoiceOver;
  bool showDots = false;
  bool showReplay = false;
  bool showPause = false;
  bool playDuringTransition = false;
  bool hidePlayPauseButton = false;
  ControlAlignment controlAlignment;
  VoidCallback? onReplayVideo;
  Function(bool)? onPausePlayVideo;
  VoidCallback? onEndVideo;

  late Timer _timer = Timer(const Duration(seconds: 0), () {});
  late Timer _videoTimer = Timer(const Duration(seconds: 0), () {});

  int _elapsedTime = 0;
  int _elapsedVideoDuration = 0;

  ValueNotifier<int> overlayNotifier = ValueNotifier(0);
  ValueNotifier<bool> playingNotifier = ValueNotifier(false);

  bool get isFullScreen => _isFullScreen;

  bool _mounted = false;

  bool get mounted => _mounted;

  bool _isPlaying = false;

  Future _initialize() async {
    _mounted = true;
    _elapsedTime = 0;
    _isPlaying = autoPlay;
    playingNotifier.value = autoPlay;

    debugPrint('Initialise video timer');
    _videoTimer =
        Timer.periodic(const Duration(milliseconds: 100), (timer) async {
      if (videoPlayerController.value.isPlaying) {
        _elapsedVideoDuration += 100;
        final elapsedDuration = Duration(milliseconds: _elapsedVideoDuration);

        debugPrint('video sec: ' +
            videoPlayerController.value.toString() +
            ' elapsed: ' +
            elapsedDuration.toString() +
            ' endTime: ' +
            endTime.toString());
        if (elapsedDuration.inSeconds == (endTime! - startTime!) ||
            (endTime == -1 &&
                elapsedDuration.inSeconds >=
                    videoPlayerController.value.duration.inSeconds)) {
          if (looping) {
            debugPrint(
                'seeking to ' + (startTime != null ? startTime : 0).toString());
            videoPlayerController
                .seekTo(Duration(seconds: startTime != null ? startTime : 0));
            _elapsedVideoDuration = 0;
          } else {
            videoPlayerController.pause();
            playingNotifier.value = false;
            if (onEndVideo != null) {
              onEndVideo!();
            }
          }
        }
      }
    });

    if (delayDuration > 0) {
      overlayNotifier.value = delayDuration;
      this.overlay = Positioned.fill(
        child: ValueListenableBuilder<int>(
            valueListenable: overlayNotifier,
            builder: (context, value, child) {
              return ValueListenableBuilder<bool>(
                  valueListenable: playingNotifier,
                  builder: (context, isPlaying, child) {
                    return Stack(
                      children: <Widget>[
                        Positioned.fill(
                          child: AnimatedOpacity(
                            duration: const Duration(milliseconds: 300),
                            curve: Curves.easeOut,
                            opacity: value > 0 ? 1 : 0,
                            child: Container(
                              color: showReplay
                                  ? Colors.black.withOpacity(0.2)
                                  : Colors.black26,
                              child: showReplay && controlAlignment == null
                                  ? Padding(
                                      padding: EdgeInsets.only(top: 8.0),
                                      child: Align(
                                          alignment: Alignment.topCenter,
                                          child: _changeWidget(value)),
                                    )
                                  : Center(child: _changeWidget(value)),
                            ),
                          ),
                        ),
                        if (value <= 0 && !looping)
                          Positioned(
                            top: controlAlignment == ControlAlignment.TopLeft ||
                                    controlAlignment ==
                                        ControlAlignment.TopRight
                                ? 8.0
                                : null,
                            right:
                                controlAlignment == ControlAlignment.TopRight ||
                                        controlAlignment ==
                                            ControlAlignment.BottomRight
                                    ? 8.0
                                    : null,
                            left:
                                controlAlignment == ControlAlignment.TopLeft ||
                                        controlAlignment ==
                                            ControlAlignment.BottomLeft
                                    ? 8.0
                                    : null,
                            bottom: controlAlignment ==
                                        ControlAlignment.BottomLeft ||
                                    controlAlignment ==
                                        ControlAlignment.BottomRight
                                ? 8.0
                                : null,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                if (showPause)
                                  GestureDetector(
                                    onTap: () {
                                      if (isPlaying) {
                                        videoPlayerController.pause();
                                        playingNotifier.value = false;
                                      } else {
                                        if (endTime > 0 &&
                                            Duration(
                                                        milliseconds:
                                                            _elapsedVideoDuration)
                                                    .inSeconds >=
                                                (endTime! - startTime!)) {
                                          videoPlayerController.seekTo(Duration(
                                              seconds: startTime != null
                                                  ? startTime
                                                  : 0));
                                          _elapsedVideoDuration = 0;
                                        }
                                        videoPlayerController.play();
                                        playingNotifier.value = true;
                                      }
                                      if (onPausePlayVideo != null) {
                                        onPausePlayVideo!(playingNotifier.value);
                                      }
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Icon(
                                        isPlaying
                                            ? Icons.pause
                                            : Icons.play_arrow,
                                        color: Color(0xFF222F3C),
                                      ),
                                    ),
                                  ),
                                if (showReplay)
                                  GestureDetector(
                                    onTap: () {
                                      videoPlayerController.seekTo(Duration(
                                          seconds: startTime != null
                                              ? startTime
                                              : 0));
                                      _elapsedVideoDuration = 0;
                                      videoPlayerController.play();
                                      playingNotifier.value = true;
                                      if (onReplayVideo != null) {
                                        onReplayVideo!();
                                      }
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Icon(
                                        Icons.replay,
                                        color: Color(0xFF222F3C),
                                      ),
                                    ),
                                  ),
                              ],
                            ),
                          )
                      ],
                    );
                  });
            }),
      );
      notifyListeners();
    } else if (!looping && showReplay) {
      this.overlay = ValueListenableBuilder<bool>(
          valueListenable: playingNotifier,
          builder: (context, isPlaying, child) {
            return Positioned(
              top: controlAlignment == ControlAlignment.TopLeft ||
                      controlAlignment == ControlAlignment.TopRight
                  ? 8.0
                  : null,
              right: controlAlignment == ControlAlignment.TopRight ||
                      controlAlignment == ControlAlignment.BottomRight
                  ? 8.0
                  : null,
              left: controlAlignment == ControlAlignment.TopLeft ||
                      controlAlignment == ControlAlignment.BottomLeft
                  ? 8.0
                  : null,
              bottom: controlAlignment == ControlAlignment.BottomLeft ||
                      controlAlignment == ControlAlignment.BottomRight
                  ? 8.0
                  : null,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  if (showPause)
                    GestureDetector(
                      onTap: () {
                        if (isPlaying) {
                          videoPlayerController.pause();
                          playingNotifier.value = false;
                        } else {
                          if (endTime! > 0 &&
                              Duration(milliseconds: _elapsedVideoDuration)
                                      .inSeconds >=
                                  (endTime! - startTime!)) {
                            videoPlayerController.seekTo(Duration(
                                seconds: startTime != null ? startTime : 0));
                            _elapsedVideoDuration = 0;
                          }
                          videoPlayerController.play();
                          playingNotifier.value = true;
                        }
                        if (onPausePlayVideo != null) {
                          onPausePlayVideo!(playingNotifier.value);
                        }
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(
                          isPlaying ? Icons.pause : Icons.play_arrow,
                          color: Color(0xFF222F3C),
                        ),
                      ),
                    ),
                  if (showReplay)
                    GestureDetector(
                      onTap: () {
                        videoPlayerController.seekTo(Duration(
                            seconds: startTime != null ? startTime : 0));
                        _elapsedVideoDuration = 0;
                        videoPlayerController.play();
                        playingNotifier.value = true;
                        if (onReplayVideo != null) {
                          onReplayVideo!();
                        }
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(Icons.replay, color: Color(0xFF222F3C)),
                      ),
                    ),
                ],
              ),
            );
          });
      notifyListeners();
    }

    if ((autoInitialize || autoPlay) &&
        !videoPlayerController.value.isInitialized) {
      await videoPlayerController.initialize();
    }

    if (startTime != null) {
      await videoPlayerController.seekTo(Duration(seconds: startTime!));
    }

    if (autoPlay) {
      if (fullScreenByDefault) {
        enterFullScreen();
      }

      _elapsedVideoDuration = 0;
      await videoPlayerController.play();
    }

    if (fullScreenByDefault) {
      videoPlayerController.addListener(_fullScreenListener);
    }
  }

  @override
  void dispose() {
    super.dispose();
    _mounted = false;
    _videoTimer?.cancel();
  }

  Widget _changeWidget(int value) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          value > 0
              ? startCommand.toString()
              : playDuringTransition ? '' : endCommand.toUpperCase(),
          style: TextStyle(
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w500,
            fontSize: 24,
            color: Colors.white,
          ),
        ),
        if (value > 0 && showDots)
          JumpingDotsProgressIndicator(
            fontSize: 24,
            color: Colors.white,
          ),
      ],
    );
  }

  void _fullScreenListener() async {
    if (videoPlayerController.value.isPlaying && !_isFullScreen) {
      enterFullScreen();
      videoPlayerController.removeListener(_fullScreenListener);
    }
  }

  void enterFullScreen() {
    _isFullScreen = true;
    notifyListeners();
  }

  void exitFullScreen() {
    _isFullScreen = false;
    notifyListeners();
  }

  void toggleFullScreen() {
    _isFullScreen = !_isFullScreen;
    notifyListeners();
  }

  void toggleVoiceOver(bool enable) {
    allowVoiceOver = enable;
    notifyListeners();
  }

  void toggleReplay(bool show) {
    showReplay = show;
    notifyListeners();
  }

  Future<void> play({bool voiceOver = false, bool ignoreTransition = false}) async {
    _isPlaying = true;

    if (playCallback != null) {
      playCallback!(true);
    }
    if (voiceOver == null) {
      voiceOver = allowVoiceOver;
    }

    if (startTime != null) {
      await videoPlayerController.seekTo(Duration(seconds: startTime));
    }

    if (delayDuration > 0 && !ignoreTransition) {
      if (playDuringTransition) {
        if (endTime > 0 &&
            Duration(milliseconds: _elapsedVideoDuration).inSeconds >=
                (endTime - startTime)) {
          videoPlayerController
              .seekTo(Duration(seconds: startTime != null ? startTime : 0));
          _elapsedVideoDuration = 0;
        }
        await videoPlayerController.play();
        playingNotifier.value = true;
        this.hidePlayPauseButton = false;
      }
      if (_elapsedTime >= 0) {
        speakGetReady(voiceOver);

        _timer?.cancel();
        _timer = Timer.periodic(const Duration(seconds: 1), (timer) async {
          overlayNotifier.value = delayDuration - _elapsedTime;
          _elapsedTime += 1;
          if (_elapsedTime > delayDuration) {
            speakGo(voiceOver);
            _timer?.cancel();
            overlayNotifier.value = -1;
            _elapsedTime = -1;
            if (!playDuringTransition) {
              if (endTime > 0 &&
                  Duration(milliseconds: _elapsedVideoDuration).inSeconds >=
                      (endTime - startTime)) {
                videoPlayerController.seekTo(
                    Duration(seconds: startTime != null ? startTime : 0));
                _elapsedVideoDuration = 0;
              }
              await videoPlayerController.play();
              playingNotifier.value = true;
            }
          }
          notifyListeners();
          this.hidePlayPauseButton = false;
        });
      }
    } else {
      if (endTime > 0 &&
          Duration(milliseconds: _elapsedVideoDuration).inSeconds >=
              (endTime - startTime)) {
        videoPlayerController
            .seekTo(Duration(seconds: startTime != null ? startTime : 0));
        _elapsedVideoDuration = 0;
      }
      await videoPlayerController.play();
      playingNotifier.value = true;
      notifyListeners();
      this.hidePlayPauseButton = false;
    }
  }

  void speakGetReady(bool voiceOver) async {
    if (!voiceOver || tts == null) return;
    await tts?.speak(startCommand);
  }

  void speakGo(bool voiceOver) async {
    if (!voiceOver || tts == null) return;
    await tts?.speak(endCommand);
  }

  void setStartTime(int startTime) {
    this.startTime = startTime;
  }

  void setEndTime(int endTime) {
    this.endTime = endTime;
  }

  Future<void> setLooping(bool looping) async {
//    await videoPlayerController.setLooping(looping);
  }

  Future<void> pause() async {
    _isPlaying = false;

    if (playCallback != null) {
      playCallback!(false);
    }

    _timer?.cancel();
    await videoPlayerController.pause();
    playingNotifier.value = false;
    if (mounted) notifyListeners();
  }

  Future<void> seekTo(Duration moment, {bool ignoreTransition = false}) async {
    if (ignoreTransition) {
      videoPlayerController
          .seekTo(Duration(seconds: startTime != null ? startTime : 0));
      videoPlayerController.play();
      playingNotifier.value = true;
      notifyListeners();
    } else {
      if (moment.inSeconds == 0 && delayDuration > 0) {
        overlayNotifier.value = delayDuration;
        _elapsedTime = 0;
        notifyListeners();
      }

      await videoPlayerController.seekTo(moment);
    }
  }

  Future<void> setVolume(double volume) async {
    await videoPlayerController.setVolume(volume);
  }

  bool get isPlaying => _isPlaying;
}

class _ChewieControllerProvider extends InheritedWidget {
  const _ChewieControllerProvider({
    Key? key,
    required this.controller,
    required Widget child,
  })  : super(key: key, child: child);

  final ChewieController controller;

  @override
  bool updateShouldNotify(_ChewieControllerProvider old) =>
      controller != old.controller;
}
